import { IsNotEmpty, MinLength, IsPositive, IsInt } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  @IsInt()
  price: number;
}
