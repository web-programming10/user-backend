import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'ส้มตำทะเล', price: 100 },
  { id: 2, name: 'แกงเขียวหวาน', price: 200 },
  { id: 3, name: 'ต้มยำไก่', price: 300 },
];
let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    // const newProduct = new Product();
    // newProduct.id = lastProductId++;
    // newProduct.name = createProductDto.name;
    // newProduct.price = createProductDto.price;
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return products[index];
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'ส้มตำทะเล', price: 100 },
      { id: 2, name: 'แกงเขียวหวาน', price: 200 },
      { id: 3, name: 'ต้มยำไก่', price: 300 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
